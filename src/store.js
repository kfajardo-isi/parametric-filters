import Vue from 'vue'
import Vuex from 'vuex'
import api from './api/index'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // allData: [],
    allFilters: [],
    filterPayload: [],
    allRows: [],
    count: null
  },

  mutations: {
    // setallData: (state, allData) => (state.allData = allData),
    setallFilter: (state, allFilters) => (state.allFilters = allFilters),
    setRows: (state, allRows) => (state.allRows = allRows),
    updateCount: (state, count) => (state.count = count)
  },

  actions: {
    async fetchAllFilters ({ commit }) {
      const filters = await api.getData({
        'searchString': '*',
        'facetFilters': [],
        'orderBy': [],
        'page': 1,
        'resultsPerPage': 25
      })
      commit('setallFilter', filters.data.FacetFilters)
      console.log(filters.data)
    },
    async fetchAllRows ({ commit }, filters) {
      try {
        const rows = await api.getData(filters)
        commit('setRows', rows.data)
        console.log(rows.data.Count)
      } catch (err) {
        console.log(err)
        console.log('Count = 0')
      }
    }
  },
  getters: {
    filterName: state => name => {
      const filternames = state.allFilters.find(filterName => filterName.Name === name)
      console.log(filternames)
      return filternames
    }

  }

})
