# Overview
Abracon parametric search VueJS client application.

Note: If you somehow re-initialize **git** _(removing .git and initialize with `git init`)_, make sure to run `node node_modules/yorkie/bin/install.js` to add **githooks** capability.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
