import axios from 'axios'

const baseUrl = 'http://localhost:12346/robbietestengine'

export default {
  getData (payload) {
    let url = `${baseUrl}/parametric`
    return axios.post(url, payload)
  }
}
